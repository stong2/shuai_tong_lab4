     .data 0x10010000
m1:   .asciiz "Please enter a non negative integer number: "
      .text
      .globl main
main: # must save $ra since I’ll have a call
      sub $sp, $sp ,4
      sw $ra, 4($sp)
      # print a message
GetInteger:      
      li $v0, 4         # system call for print_str
      la $a0, m1      # address of string to print
      syscall
      andi $a0, 0
      # get a integer
       li $v0, 5         # system call for read_int
       syscall
      add $t0, $v0, 0 # first integer in $t0
      bltz $t0, GetInteger

      
# get the right number and get into Factorial
      add $a0, $a0, $t0

      jal Factorial # call ‘test’ with no parameters
      addu $t0, $v0, 0
      li $v0, 1 # system call for print_int
      addu $a0, $t0, $0 # move number to print in $a0
      syscall
      # restore the return address in $ra
      andi $t0, 0
      andi $t0, 0
      lw $ra, 4($sp)
      add $sp, $sp , 4

      jr $ra # return from main
# This procedure computes the factorial of a non-negative integer
# The parameter (an integer) received in $a0
# The result (a 32 bit integer) is returned in $v0
# The procedure uses none of the registers $s0 - $s7 so no need to save them
# Any parameter that will make the factorial compute a result larger than
# 32 bits will return a wrong result.
Factorial:
      subu $sp, $sp, 4
      sw $ra, 4($sp) # save the return address on stack
      beqz $a0, terminate # test for termination
      subu $sp, $sp, 4 # do not terminate yet
      sw $a0, 4($sp) # save the parameter
      sub $a0, $a0, 1 # will call with a smaller argument
      jal Factorial
      # after the termination condition is reached these lines
      # will be executed
      lw $t0, 4($sp) # the argument I have saved on stack
      mul $v0, $v0, $t0 # do the multiplication
      lw $ra, 8($sp) # prepare to return
      addu $sp, $sp, 8 # I’ve popped 2 words (an address and
      jr $ra # .. an argument)
terminate:
      li $v0, 1 # 0! = 1 is the return value
      lw $ra, 4($sp) # get the return address
      addu $sp, $sp, 4 # adjust the stack pointer
      jr $ra # return