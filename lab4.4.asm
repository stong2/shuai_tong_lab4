      .data 0x10010000
m1: .asciiz "Please enter two integer number: "

      .text
      .globl main
main: # must save $ra since I’ll have a call
      sub $sp, $sp ,4
      sw $ra, 4($sp)
       
      # print a message
      li $v0, 4         # system call for print_str
      la $a0, m1      # address of string to print
      syscall
      # get two integer
      li $v0, 5         # system call for read_int
      syscall
      addu $t0, $v0, $0 # first integer in $t0
      li $v0, 5         # system call for read_int
      syscall
      addu $t1, $v0, $0    #second integer in $t1
      sub $sp, $sp ,8
      sw $t0, 4($sp)
      sw $t1, 8($sp)
      jal Largest # call ‘Largest’
      # execute this after ‘largest’ returns
      lw $t0, 4($sp)
      add $sp, $sp , 4
      li $v0, 1 # system call for print_int
      addu $a0, $t0, $0 # move number to print in $a0
      syscall

      lw $ra, 4($sp)
      add $sp, $sp , 4

      jr $ra # return from main
Largest:
      lw $t0, 4($sp)
      lw $t1, 8($sp)
      add $sp, $sp , 8
      sub $t3, $t1, $t0
      bgtz $t3, Else # go to Else if $t1 - $t0 >0
      # code for block #1
      beq $0, $0, Exit # go to Exit (skip code for block #2)
      Else:
      # code for block #2
      addi $t0, $t1 , 0
      Exit:
      sub $sp, $sp , 4
      sw $t0, 4($sp)
      jr $ra # return from this procedure