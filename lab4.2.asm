.text
.globl main
main: # must save $ra since I’ll have a call
      sub $sp, $sp ,4
      sw $ra, 4($sp)
       
      jal test # call ‘test’ with no parameters
      nop # execute this after ‘test’ returns
      # restore the return address in $ra
      lw $ra, 4($sp)
      add $sp, $sp , 4

      jr $ra # return from main
test: 
      nop # this is the procedure named ‘test’
      jr $ra # return from this procedure